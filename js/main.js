$(document).ready(function(){
    $('.testmonials-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: true,
        fade: true,
        customPaging: function(slider, i) {
            return '<div class="dot"></div>';
        },
    });

    $('.slider-header').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: false,
    });

    $('.screen .screen-slide:first-child').fadeIn(); // screen first select
    $('.select-box').click(function(){
        $('.screen .screen-slide').hide()
        $('#'+$(this).data('value')).fadeIn();
    });

});